<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 13.11.2015
 * Time: 17:53
 */

namespace Language;


interface LanguageBatchBoInterface
{
    public function generateLanguageFiles();

    public function generateAppletLanguageXmlFiles();
}