<?php

namespace Language;

use Language\Api\ApiClient;
use Language\Generator\AppletLanguageXmlFilesGenerator;
use Language\Generator\LanguageFilesGenerator;
use Language\Logger\AppLoggerModeEnum;
use Language\Logger\AppLogger;
use Language\Logger\AppLoggerOutputEnum;

/**
 * Business logic related to generating language files.
 */
class LanguageBatchBo implements LanguageBatchBoInterface
{
    /**
     * @var LanguageFilesGenerator
     */
    private $languageFilesGenerator;

    /**
     * @var AppletLanguageXmlFilesGenerator
     */
    private $appletLanguageXmlFilesGenerator;

    public function __construct()
    {
        $appLogger = new AppLogger(new AppLoggerOutputEnum(AppLoggerOutputEnum::SCREEN));
        $appLogger->setMode(new AppLoggerModeEnum(AppLoggerModeEnum::INFO));
        $apiClient = new ApiClient();
        $cachePath = Config::get('system.paths.root') . '/cache/';
        $this->languageFilesGenerator = new LanguageFilesGenerator($appLogger, $apiClient, $cachePath, Config::get('system.translated_applications'));
        $this->appletLanguageXmlFilesGenerator = new AppletLanguageXmlFilesGenerator($appLogger, $apiClient, $cachePath . '/flash/', array(
            'memberapplet' => 'JSM2_MemberApplet',
        ));
    }

    public function generateLanguageFiles()
    {
        $this->languageFilesGenerator->generate();
    }

    public function generateAppletLanguageXmlFiles()
    {
        $this->appletLanguageXmlFilesGenerator->generate();
    }
}
