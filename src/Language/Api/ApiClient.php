<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 14.11.2015
 * Time: 15:21
 */

namespace Language\Api;


class ApiClient
{
    public function call($target, $mode, array $getParameters, array $postParameters)
    {
        $result = ApiCall::call($target, $mode, $getParameters, $postParameters);
        $this->checkForApiErrorResult($result);
        return $result['data'];
    }

    private function checkForApiErrorResult($result)
    {
        // Error during the api call.
        if ($result === false || !isset($result['status'])) {
            throw new ApiClientException('Error during the api call');
        }
        // Wrong response.
        if ($result['status'] != 'OK') {
            throw new ApiClientException('Wrong response: '
                . (!empty($result['error_type']) ? 'Type(' . $result['error_type'] . ') ' : '')
                . (!empty($result['error_code']) ? 'Code(' . $result['error_code'] . ') ' : '')
                . ((string)$result['data']));
        }
        // Wrong content.
        if ($result['data'] === false) {
            throw new ApiClientException('Wrong content!');
        }
    }
}