<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 13.11.2015
 * Time: 20:02
 */

namespace Language\Generator;


use Language\Api\ApiClient;
use Language\Logger\AppLogger;

class AppletLanguageXmlFilesGenerator extends AbstractFilesGenerator
{
    private $applets;

    public function __construct(AppLogger $logger, ApiClient $apiClient, $path, array $applets)
    {
        parent::__construct($logger, $apiClient, $path);
        $this->applets = $applets;
    }

    final public function generate()
    {
        $this->appLogger->log('Generating applet language XML files:', $this->applets);

        foreach ($this->applets as $appletDirectory => $appletLanguageId) {
            $this->appLogger->log("\t[APPLETDIRECTORY: $appletDirectory]");
            $this->appLogger->log("\t\tGetting languages for appletLanguageId $appletLanguageId");

            $languages = $this->getAppletLanguages($appletLanguageId);
            $this->appLogger->log("\t\t\t" . '[OK] Available languages: ' . implode(', ', $languages));

            foreach ($languages as $language) {
                $this->appLogger->log("\t\t" . '[LANGUAGE: ' . $language . ']');
                $fileName = $this->generateFile($appletLanguageId, $language);
                $this->appLogger->log("\t\t\t" . '[OK] ' . $fileName);
            }
        }

        $this->appLogger->log('Applet language XML files generated.');
    }

    private function generateFile($applet, $language)
    {
        $content = $this->getAppletLanguageFile($applet, $language);
        $fileName = '/lang_' . $language . '.xml';
        $this->save($fileName, $content);
        return $fileName;
    }

    private function getAppletLanguageFile($applet, $language)
    {
        return $this->apiClient->call(
            'system_api',
            'language_api',
            array(
                'system' => 'LanguageFiles',
                'action' => 'getAppletLanguageFile'
            ),
            array(
                'applet' => $applet,
                'language' => $language
            )
        );
    }

    private function getAppletLanguages($applet)
    {
        $languages = $this->apiClient->call(
            'system_api',
            'language_api',
            array(
                'system' => 'LanguageFiles',
                'action' => 'getAppletLanguages'
            ),
            array('applet' => $applet)
        );

        if (empty($languages)) {
            throw new FilesGeneratorException('There is no available languages for the ' . $applet . ' applet.');
        }
        return $languages;
    }
}