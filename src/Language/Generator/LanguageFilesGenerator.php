<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 13.11.2015
 * Time: 19:59
 */

namespace Language\Generator;


use Language\Api\ApiClient;
use Language\Logger\AppLogger;

class LanguageFilesGenerator extends AbstractFilesGenerator
{
    private $applications;

    public function __construct(AppLogger $logger, ApiClient $apiClient, $path, array $applications)
    {
        parent::__construct($logger, $apiClient, $path);
        $this->applications = $applications;
    }

    final public function generate()
    {
        $this->appLogger->log('Generating language files:', $this->applications);
        foreach ($this->applications as $application => $languages) {
            $this->appLogger->log("\t" . '[APPLICATION: ' . $application . ']');

            foreach ($languages as $language) {
                $this->appLogger->log("\t\t" . '[LANGUAGE: ' . $language . ']');

                $fileName = $this->generateFile($application, $language);
                $this->appLogger->log("\t\t\t" . '[OK] ' . $fileName);
            }
        }

        $this->appLogger->log('Language files generated.' . PHP_EOL);
    }

    private function generateFile($application, $language)
    {
        $content = $this->getLanguageFile($application, $language);
        $fileName = $application . '/' . $language . '.php';
        $this->save($fileName, $content);
        return $fileName;
    }

    private function getLanguageFile($application, $language)
    {
        return $this->apiClient->call(
            'system_api',
            'language_api',
            array(
                'system' => 'LanguageFiles',
                'action' => 'getLanguageFile'
            ),
            array('language' => $language)
        );
    }


}