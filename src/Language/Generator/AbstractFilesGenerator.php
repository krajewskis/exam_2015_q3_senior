<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 13.11.2015
 * Time: 20:04
 */

namespace Language\Generator;


use Language\Api\ApiClient;
use Language\Logger\AppLogger;

abstract class AbstractFilesGenerator
{
    /**
     * @var AppLogger
     */
    protected $appLogger;

    /**
     * @var ApiClient
     */
    protected $apiClient;

    private $path;

    protected function __construct(AppLogger $logger, ApiClient $apiClient, $path)
    {
        $this->appLogger = $logger;
        $this->apiClient = $apiClient;
        $this->path = $path;
    }

    abstract public function generate();

    protected function save(&$fileName, $content)
    {
        $fileName = $this->path . '/' . $fileName;
        $dirName = dirname($fileName);

        if (!is_dir($dirName)) {
            mkdir($dirName, 0755, true);
        }

        $bytes = file_put_contents($fileName, $content);

        if ($bytes === 0 || strlen($content) !== $bytes) {
            throw new FilesGeneratorException ('Unable to save language file ' . $fileName);
        }
    }
}