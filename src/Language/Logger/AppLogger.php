<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 17.11.2015
 * Time: 14:21
 */

namespace Language\Logger;


class AppLogger
{
    /**
     * @var AppLoggerOutputEnum
     */
    private $output;

    /**
     * @var AppLoggerModeEnum
     */
    private $mode;

    public function __construct(AppLoggerOutputEnum $output)
    {
        $this->setOutput($output);
        $this->setMode(new AppLoggerModeEnum(AppLoggerModeEnum::SILENT));
    }

    public function setOutput(AppLoggerOutputEnum $output)
    {
        $this->output = $output;
        if ($this->output->equals(AppLoggerOutputEnum::LOGFILE)) {
            throw new \Exception('Log to file not implemented.');
        }
    }

    public function setMode(AppLoggerModeEnum $mode)
    {
        $this->mode = $mode;
        if ($this->mode->equals(AppLoggerModeEnum::DEBUG)) {
            $this->output('DEBUG mode enabled.' . PHP_EOL);
        }
    }

    public function log($message, array $parameters = null)
    {
        switch ($this->mode->getValue()) {
            case AppLoggerModeEnum::SILENT;
                break;

            case AppLoggerModeEnum::INFO;
                $this->output($message, PHP_EOL);
                break;

            case AppLoggerModeEnum::DEBUG;
                if ($parameters && count($parameters) > 0) {
                    $this->output($message, PHP_EOL, print_r($parameters, true), PHP_EOL);
                } else {
                    $this->output($message, PHP_EOL);
                }

                break;
        }
    }

    private function output()
    {
        switch ($this->output->getValue()) {
            case AppLoggerOutputEnum::LOGFILE;
                //TODO implement log to file
                break;

            case AppLoggerOutputEnum::SCREEN;
                foreach (func_get_args() as $arg) {
                    echo $arg;
                }
                break;
        }
    }
}