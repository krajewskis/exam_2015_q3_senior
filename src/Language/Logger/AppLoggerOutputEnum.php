<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 17.11.2015
 * Time: 15:16
 */

namespace Language\Logger;


use Language\Enum\Enum;

class AppLoggerOutputEnum extends Enum
{
    const SCREEN = 0;
    const LOGFILE = 1;
}