<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 17.11.2015
 * Time: 14:27
 */

namespace Language\Logger;


use Language\Enum\Enum;

class AppLoggerModeEnum extends Enum
{
    const SILENT = 0;
    const INFO = 1;
    const DEBUG = 2;
}