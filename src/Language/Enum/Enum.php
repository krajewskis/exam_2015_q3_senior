<?php
/**
 * Created by PhpStorm.
 * User: krajewski
 * Date: 18.9.14
 * Time: 17:38
 */

namespace Language\Enum;


abstract class Enum
{
	public $value;

	public function __construct($value)
	{
		$values = self::getValues();

		if (!in_array($value, $values)) {
			throw new \UnexpectedValueException();
		}

		$this->value = $value;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function equals($value)
	{
		return $this->value === $value;
	}

	public function __toString()
	{
		return (string)$this->value;
	}

	public static function getValues()
	{
		$calledClass = get_called_class();
		$reflectionClass = new \ReflectionClass ($calledClass);
		$array = $reflectionClass->getConstants();
		return $array;
	}

	public static function checkValue($value)
	{
		$values = self::getValues();
		if (in_array($value, $values)) {
			return true;
		}
		return false;
	}
} 