<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 13.11.2015
 * Time: 21:03
 */

namespace Language\Test;


use Language\Config;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    public function testGet_system_paths_root()
    {
        $val = Config::get('system.paths.root');
        $this->assertEquals(realpath(dirname(__FILE__) . '/../../../'), $val);
    }

    public function testGet_system_translateda_pplications()
    {
        $val = Config::get('system.translated_applications');
        $this->assertEquals(['portal' => ['en', 'hu']], $val);
    }

    public function testGet_default()
    {
        $val = Config::get('');
        $this->assertNull($val);
    }
}