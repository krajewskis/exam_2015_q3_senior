<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 17.11.2015
 * Time: 16:04
 */

namespace Language\Test\Logger;


use Language\Logger\AppLogger;
use Language\Logger\AppLoggerModeEnum;
use Language\Logger\AppLoggerOutputEnum;

class AppLoggerTest extends \PHPUnit_Framework_TestCase
{
    const TEST_MESSAGE = 'test message';

    /**
     * @var AppLogger
     */
    private $logger;

    public function setUp()
    {
        parent::setUp();
        $this->logger = new AppLogger(new AppLoggerOutputEnum(AppLoggerOutputEnum::SCREEN));
    }

    public function testLog_silent()
    {
        $this->logger->setMode(new AppLoggerModeEnum(AppLoggerModeEnum::SILENT));

        ob_start();
        $this->logger->log(self::TEST_MESSAGE);

        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEmpty($output);
    }

    public function testLog_info()
    {
        $this->logger->setMode(new AppLoggerModeEnum(AppLoggerModeEnum::INFO));

        ob_start();
        $this->logger->log(self::TEST_MESSAGE);

        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals(self::TEST_MESSAGE, trim($output));
    }

    /**
     * @expectedException \Exception
     */
    public function test_not_implemented_file_mode()
    {
        $this->logger->setOutput(new AppLoggerOutputEnum(AppLoggerOutputEnum::LOGFILE));
    }
}