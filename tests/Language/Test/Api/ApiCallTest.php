<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 13.11.2015
 * Time: 21:13
 */

namespace Language\Test\Api;


use Language\Api\ApiCall;

class ApiCallTest extends \PHPUnit_Framework_TestCase
{
    public function testCallNotExistAction()
    {
        $response = ApiCall::call(null, null, array(), array());
        $this->assertNull($response);
    }

    public function testCall_getLanguageFile()
    {
        $response = ApiCall::call(null, null, array('action' => 'getLanguageFile'), null);
        $this->assertEquals('OK', $response['status']);
        $this->assertEquals(ApiCall::GET_LANGUAGE_FILE_RESULT, $response['data']);
    }

    public function testCall_getAppletLanguages()
    {
        $response = ApiCall::call(null, null, array('action' => 'getAppletLanguages'), null);
        $this->assertEquals('OK', $response['status']);
        $this->assertEquals(['en'], $response['data']);
    }

    public function testCall_getAppletLanguageFile()
    {
        $response = ApiCall::call(null, null, array('action' => 'getAppletLanguageFile'), null);
        $this->assertEquals('OK', $response['status']);
        $this->assertEquals(ApiCall::GET_APPLET_LANGUAGE_FILE_RESULT, $response['data']);
    }
}