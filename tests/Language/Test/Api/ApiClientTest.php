<?php
/**
 * Created by PhpStorm.
 * User: kraje
 * Date: 14.11.2015
 * Time: 15:43
 */

namespace Language\Test\Api;


use Language\Api\ApiCall;
use Language\Api\ApiClient;

class ApiClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ApiClient
     */
    private $apiClient;

    public function setUp()
    {
        parent::setUp();
        $this->apiClient = new ApiClient();
    }

    /**
     * @expectedException \Language\Api\ApiClientException
     */
    public function testCallNotExistAction()
    {
        $this->apiClient->call(null, null, array(), array());
    }

    public function testCall_getLanguageFile()
    {
        $data = $this->apiClient->call(null, null, array('action' => 'getLanguageFile'), array());
        $this->assertEquals(ApiCall::GET_LANGUAGE_FILE_RESULT, $data);
    }

    public function testCall_getAppletLanguages()
    {
        $data = $this->apiClient->call(null, null, array('action' => 'getAppletLanguages'), array());
        $this->assertEquals(['en'], $data);
    }

    public function testCall_getAppletLanguageFile()
    {
        $data = $this->apiClient->call(null, null, array('action' => 'getAppletLanguageFile'), array());
        $this->assertEquals(ApiCall::GET_APPLET_LANGUAGE_FILE_RESULT, $data);
    }
}